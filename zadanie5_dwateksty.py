
#!/usr/bin/env python
# -*- coding: utf-8 -*-'
import string

file1 = open("tekst1.txt", "r")
file2 = open("tekst2.txt", "r")

linia1 = file1.readlines()
linia2 = file2.readlines()

str1 = ''.join(str(e) for e in linia1)
str1 = str1.lower()
str2 = ''.join(str(e) for e in linia2)
str2 = str2.lower()
tab1 = str1.split()
tab1.sort()
tab2 = str2.split()
tab2.sort()

for i in tab1:
    malei = i.lower()
    for j in tab2:
        malej = j.lower()
        if malei==malej:
            print("Znaleziono!", i, j)
        
file1.close()
file2.close()