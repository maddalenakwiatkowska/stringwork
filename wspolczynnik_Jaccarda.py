 
import unittest

class Jaccardsim():

    def otwieranie(self):
        loop = "qq"
        while loop=="qq":
            x = input("Do you want to work with A- letters, B- words?: ")
            
            if x =="A" or x =="a":
                loop =="q"
                f1 = open("Jaccardlitery1.txt", "r")
                f2 = open("Jaccardlitery2.txt", "r")
                fread1 = f1.readlines()
                fread2 = f2.readlines()
                self.tokenlitery(fread1,fread2)
                return loop
            elif x == "B" or x == "b":
                loop == "q"
                f1 = open("Jaccardwyrazy1.txt", "r")
                f2 = open("Jaccardwyrazy2.txt", "r")
                fread1 = f1.readlines()
                fread2 = f2.readlines()
                self.tokenwyrazy(fread1,fread2)
                return loop
            else: 
                print("Wrong answer")    

    def tokenlitery(self, fread1, fread2):
        
        str1 = ''.join(str(e) for e in fread1)
        str2 = ''.join(str(e) for e in fread2)
        f1 = open("wspolczynnik_jaccarda1.txt", "w")
        f2 = open("wspolczynnik_jaccarda2.txt", "w")
        self.slicing_letters(str1)

    def slicing_letters(self,str1):
        tab1 = [e for e in range(0,0)]  
        counter_i = 0
        for i in str1:
                if i == ' ' or i == '\n':
                    continue
                if i not in tab1:
                    counter_i+=1
                    tab1.append(i)  
                    tab1.sort()     
        return tab1   
        print("I counter:", counter_i)
        #zap1 = f1.write(' '.join(tab1))    
       
    def slicing_letters2(self,str2):
        tab2 = [w for w in range(0,0)]
        intersection = 0
        counter_j = 0
        for j in str2:
            if j == ' ' or j == '\n':
                continue
            if j not in tab2:
                counter_j+=1
                tab2.append(j)  
                tab2.sort()
                if j in tab1:
                    intersection +=1
        #zap2 = f2.write(' '.join(tab2)) 
        print("J counter:", counter_j)
        print("intersection: ", intersection)
        self.count_factor(intersection, counter_i, counter_j)   

    def tokenwyrazy(self, fread1, fread2):
         
        str1 = ','.join(str(e) for e in fread1)
        str2 = ','.join(str(e) for e in fread2)
        f1 = open("wspolczynnik_jaccarda1.txt", "w")
        f2 = open("wspolczynnik_jaccarda2.txt", "w") 

        print(str1,str2)    
        str1 = str1.split(",")
        str2 = str2.split(",")
        self.slicing_words(str1)
        self.slicing_words2(str2)

    def slicing_words(self,str1):
        for i in str1:
            zapisz = i
            length = len(zapisz)
            if zapisz[0] == " ":
                a = 1
                slicing = ""
                for a in range(a,length-1): 
                    bufor = zapisz[a:][:2]
                    slicing = slicing + bufor
                return slicing
            else: 
                a = 0   
                slicing = "" 
                for a in range(a,length-1): 
                    bufor = zapisz[a:][:2]
                    slicing = slicing + bufor
                return slicing
            
    def slicing_words2(self, str2):
        for i in str2:
            zapisz = i
            length = len(zapisz)
            slicing = ""
            if zapisz[0] == " ":
                a = 1
            else:
                a = 0    
            for a in range(a,length-1): 
                bufor = zapisz[a:][:2]
                slicing = slicing + bufor
            return slicing
                
    def count_factor(self,intersection,counter_i,counter_j):    
        if counter_i<counter_j:
            result = intersection/counter_j
            print("The Jaccard index is equal: ", result)
        elif counter_i>counter_j:
            result = intersection/counter_i
            print("The Jaccard index is equal: ", result)
    
class Test_Jaccard(unittest.TestCase):
    def test_slicing(self):
        p = Jaccardsim()
        self.assertEqual(p.slicing_words(str1 = ["kobieta"]), "koobbiieetta")
        self.assertEqual(p.slicing_words(str1 =["dziecko"]), "dzziieecckko")
        self.assertEqual(p.slicing_words(str1 =["wiatr"]), "wiiaattr")
        self.assertEqual(p.slicing_words2(str2 =["wiatr"]), "wiiaattr")
        self.assertEqual(p.slicing_words2(str2 =["radosc"]), "raaddoossc")
        self.assertEqual(p.slicing_letters(str1 = "radosc"), ["a","c","d","o","r","s"])
        self.assertEqual(p.slicing_letters(str1 = "slownik"), ["i","k","l","n","o","s", "w"])

if __name__=='__main__':
    ob = Jaccardsim()
    ob.otwieranie() 
    unittest.main()